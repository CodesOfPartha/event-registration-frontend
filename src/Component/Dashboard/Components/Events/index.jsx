import React, {useState} from 'react';
import RegisterEvent from '../../Modals/RegisterModal';
import { getData, deleteData } from '../../../../Services/http_services';
import NewEventModal from '../../Modals/NewEventModal';

const Index = ({ admin, participate, registered, events }) => {

    const [regiterEvent, setRegiterEvent] = useState(false);
    const [event, setEvent] = useState(undefined);
    const [editEvent, setEditEvent] = useState(false);
    const [index, setIndex] = useState(0);

    const registerEvents = (event) => {
        if(registered){
            let confirm = window.confirm('Are you sure u want to cancel this registration');
            if(confirm){
                getData(`cancel/events/${event._id}`)
                .then((res)=>window.location='/dashboard')
                .catch((err)=> console.log(err))
            }
        }else{
            setEvent(event);
            setRegiterEvent(true);
        }
    }

    const handleEdit = (index) => {
        setEditEvent(true);
        setIndex(index);
    };

    const handleDelete = (eventId) => {
        let confirm = window.confirm('Are you sure u want to delete this event');
        if(confirm){
            deleteData(`events/${eventId}`)
            .then((res)=>console.log(res.data))
            .catch((err)=>console.log(err))
        }
    }

    return (
        <div className="row mt-2">
            <div className="col-lg-12">
                {events?.map((event,i) => <div class="card min-height-170 mb-3">
                    <div class="card-body">
                        <div className="d-flex justify-content-between">
                            <h5 className="card-title">{event.name}</h5>
                            <div>
                                {participate || registered ? <button className="btn btn-primary mr-4" onClick={()=>registerEvents(event)}>{registered ? 'Cancel Registration' : 'Register'}</button> : null}
                            </div>
                        </div>
                        <div className="d-flex justify-content-between">
                            <div>
                                <p class="card-text">{event.description}</p>
                                <div className="d-flex justify-content-between">
                                    <p>Duration: {event.duration}Days</p>
                                    <p>Fees: Rs: {event.fees}/-</p>
                                    <p>Max participant: {event.maxParticipant} Members</p>
                                </div>
                            </div>
                            {admin ? <div>
                                <p className="text-warning mt-3 mr-5 cursor-pointer" onClick={()=>handleEdit(i)}><i className="fa fa-edit"></i> Edit</p>
                                <p className="text-danger mr-5 cursor-pointer" onClick={()=>handleDelete(event._id)}><i className="fa fa-trash"></i> Delete</p>
                            </div> : null}
                        </div>
                    </div>
                </div>)}
                {
                    events ?.length === 0 ? <p>No Events yet!</p> : null
                }
                <RegisterEvent regiterEvent={regiterEvent} event={event} setRegiterEvent={setRegiterEvent} />
                <NewEventModal newEvent={editEvent} setNewEvent={setEditEvent} events={events?events[index]:undefined}/>
            </div>
        </div>

    )
}

export default Index;
