import React, { useState, useEffect } from 'react';
import { Modal, Radio, FormControlLabel, Checkbox } from '@material-ui/core';
import './style.scss';
import { postData, patchData } from '../../../../Services/http_services';

const Index = ({ newEvent, setNewEvent, events }) => {
    const temp = {
        name:'',
        description:'',
        location:'',
        fees:'',
        maxParticipant:'',
        duration:'',
        tags:[''],
        fields:[{
            fieldName:'',
            textbox:true,
            required:false
        }],
    }
    const [form, setForm] = useState(temp);

    const handleClose = () => {
        setNewEvent(false);
    };
    console.log('events=> ', events);

    useEffect(() => {
        if(events){
            setForm(events);
        }
    }, [events])

    const addNewTag = () => {
        const newForm = JSON.parse(JSON.stringify(form));
        newForm.tags.push('');
        setForm(newForm);
    }
    const addNewField = () => {
        const newForm = JSON.parse(JSON.stringify(form));
        newForm.fields.push(temp.fields[0]);
        setForm(newForm);
    }

    const modalStyle = {
        width: '40vw',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%,-50%)',
        minHeight:'80vh',
        maxHeight: '900px',
        background: '#fff',
    };

    const handleChange = (key, value) => {
        const newForm = JSON.parse(JSON.stringify(form));
        newForm[key]=value;
        setForm(newForm);
    };

    const handleTag = (index, value) => {
        const newForm = JSON.parse(JSON.stringify(form));
        newForm.tags[index]=value;
        setForm(newForm);
    };

    const handleFields = (index, key, value) => {
        const newForm = JSON.parse(JSON.stringify(form));
        newForm.fields[index][key]=value;
        setForm(newForm);
    };

    const removeFields = (index, key) => {
        
        const newForm = JSON.parse(JSON.stringify(form));
        if(newForm[key].length>1){
            newForm[key].splice(index, 1);
            setForm(newForm);
        }
    }
    const createEvent = () => {
        if(form.name !== '' && form.fees !== '' && form.description !== '' && form.location !=='' && form.maxParticipant !=='' && form.duration !=='' && form.fields[0].fieldName !== ''){
            if(events){
                patchData(`events/${events._id}`, form)
                .then((res)=>window.location='/dashboard')
                .catch((err)=>console.log(err))
            }else{
            postData(`events/create`, form)
                .then((res)=>window.location='/dashboard')
                .catch((err)=>console.log(err))
            }
        } else{
            alert('all the fields are required!');
        }
    }

    return (
        <div>
            <Modal open={newEvent} onClose={handleClose}>
                <div style={modalStyle} className="p-4">
                    <div className="d-flex justify-content-between">
                        <h5>Add new event</h5>
                        <i className="fas fa-times mt-2 cursor-pointer mr-2" onClick={handleClose}></i>
                    </div>
                    <div className="modal-table mt-2 mb-2">
                    <table className="table table-borderless">
                        <tbody>
                            <tr >
                                <td className="text-right">
                                    Name:
                            </td>
                                <td>
                                    <input type='text' value={form.name} onChange={(e)=>handleChange('name',e.target.value)} className="form-control" placeholder="Name" />
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Description:
                            </td>
                                <td>
                                    <textarea rows='2' className="form-control" onChange={(e)=>handleChange('description',e.target.value)} value={form.description}  placeholder="About the event"  />
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Location:
                            </td>
                                <td>
                                    <input type='text' onChange={(e)=>handleChange('location',e.target.value)} value={form.location} className="form-control" placeholder="Location"  />
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Fees:
                            </td>
                                <td>
                                    <input type='text' value={form.fees} onChange={(e)=>handleChange('fees',e.target.value)} className="form-control" placeholder="fees in number"  />
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Max Participant:
                                </td>
                                <td>
                                    <input type='text' value={form.maxParticipant} onChange={(e)=>handleChange('maxParticipant',e.target.value)} className="form-control" placeholder="max participants in number" />
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Duration in days:
                                </td>
                                <td>
                                    <input type='text' value={form.duration} onChange={(e)=>handleChange('duration',e.target.value)} className="form-control" placeholder="days in number"  />
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Tags:
                                </td>
                                <td>
                                    {form.tags.map((tag, i)=>{return(<div className="d-flex justify-content-between"><input type='text' value={tag} onChange={(e)=>handleTag(i,e.target.value)} className="form-control mb-2" placeholder="tag"  /><i className="fas fa-times mt-2 cursor-pointer ml-2" onClick={()=>removeFields(i, 'tags')}></i></div>)})}
                                    <p className="add-tag" onClick={addNewTag}>+Add another tag</p>
                                </td>
                            </tr>
                            <tr >
                                <td className="text-right">
                                    Participant Details:
                                </td>
                                <td>
                                    {form.fields.map((field, i)=><div>
                                        <div className="d-flex justify-content-between">
                                        <input type='text' value={field.fieldName} onChange={(e)=>handleFields(i, 'fieldName', e.target.value)} className="form-control mb-2" placeholder="field name" />
                                        <i className="fas fa-times mt-2 cursor-pointer ml-2" onClick={()=>removeFields(i, 'fields')}></i>
                                        </div>
                                        Type of input:
                                            <div className="d-flex justify-content-start">
                                                <FormControlLabel checked={field.textbox} control={<Radio color="primary" onClick={()=>handleFields(i, 'textbox', true)} />} label="Textbox" />
                                                <FormControlLabel checked={!field.textbox} control={<Radio color="primary" onClick={()=>handleFields(i, 'textbox', false)} />} label="Textarea" />
                                            </div>
                                            <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="checkedBox"
                                                    color="primary"
                                                    checked={field.required}
                                                    onClick={(e)=>handleFields(i, 'required', e.target.checked)}
                                                />
                                            }
                                            label="Required"
                                        />
                                    </div>)}
                                    <p className="add-tag" onClick={addNewField}>+Add another field</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div className="text-right">
                        <button className="btn btn-primary" onClick={createEvent}>{events?'Update event':'Create new Event'}</button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default Index;
