import React, {useState, useEffect} from 'react';
import { Modal } from '@material-ui/core';
import {getData} from '../../../../Services/http_services';

const Index = ({regiterEvent, setRegiterEvent, event}) => {

    const handleClose = () => {
        setRegiterEvent(false);
    };
    const [state, setState] = useState(undefined);
    const modalStyle = {
        width: '40vw',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%,-50%)',
        minHeight:'400px',
        maxHeight: '800px',
        background: '#fff',
        overflowY: 'auto'
    };

    const handleChange = (index, value) => {
        let temp=JSON.parse(JSON.stringify(state));
        temp.fields[index].value = value;
        setState(temp);
    };

    const registerUser = () => {
        let flag = false;
        state.fields.map((field)=>{
            if( field.required && field.value.trim()===''){
                flag=true;
            }
        })
        if(flag){
            alert('Required field is empty');
        } else {
            getData(`register/events/${state._id}`)
            .then((res)=>window.location='/dashboard')
            .catch((err)=>console.log(err))
        }
    }

    useEffect(() => {
        if(event){
            let temp=JSON.parse(JSON.stringify(event));
            temp.fields.map((field, i)=>temp.fields[i].value='')
            setState(temp);
        }
    }, [event])

    return (
        <div>
            <Modal open={regiterEvent} onClose={handleClose}>
                <div style={modalStyle} className="p-4">
                    <div className="d-flex justify-content-between">
                        <h5>Register to {event?.name}</h5>
                        <i className="fas fa-times mt-2 cursor-pointer mr-2" onClick={handleClose}></i>
                    </div>
                    <table className="table table-borderless">
                        <tbody>
                            {state?.fields.map((field, i)=><tr>
                                <td>
                                    {field.fieldName}
                                    <br />
                                    {field.required?<span className="text-danger">Required *</span>:null}
                                </td>
                                <td>
                                    <input type='text' value={field.value} onChange={(e)=>handleChange(i, e.target.value)}  className="form-control" />
                                </td>
                            </tr>)}
                        </tbody>
                    </table>
                    <div className="text-right">
                        <button className="btn btn-primary" onClick={registerUser}>Register</button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default Index;
