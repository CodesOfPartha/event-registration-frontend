import React, {useState, useEffect} from 'react';
import NewEventModal from './Modals/NewEventModal';
import { Tab, Tabs } from 'react-bootstrap';
import Events from './Components/Events';
import {getData} from '../../Services/http_services';

const Index = () => {
    const [newEvent, setNewEvent] = useState(false);
    const [key, setKey] = useState('all');
    const [eventResponse, setEventResponse] = useState(undefined);
    const [notification, setNotification] = useState(undefined);

    const handleLogout = () => {
        localStorage.clear();
        window.location = '/auth/login';
    }

    const getEvents = () => {
        getData(`events`)
        .then((res)=>setEventResponse(res.data))
        .catch((err)=>console.log(err))
        getData(`user/notification`)
        .then((res)=>setNotification(res.data))
        .catch((err)=>console.log(err))
    }

    useEffect(() => {
        getEvents()
    }, [])

    return (
        <div className="row m-5">
            <div className="col-lg-12">
                <div className="row">
                    <div className="col-lg-8">
                    <Tabs
                        id='controlled-tab-example'
                        activeKey={key}
                        onSelect={(k) => {
                            setKey(k);
                        }}
                    >
                        <Tab eventKey='all' title='Participate Events'>
                            <Events participate={true} events={eventResponse?.events.overallEvents}/>
                        </Tab>
                        <Tab eventKey='register' title='Registered Events'>
                            <Events registered={true} events={eventResponse?.events.registeredEvents} />
                        </Tab>
                        <Tab eventKey='mine' title='Your Events'>
                            <Events admin={true} events={eventResponse?.events.createdEvents}/>
                        </Tab>
                    </Tabs>
                    </div>
                    <div className="col-lg-4">
                        <div className="row">
                            <div className="col-lg-12 d-flex justify-content-end ">
                            <button className="btn btn-primary mr-3" onClick={()=>setNewEvent(true)}>Add New Event</button>
                            <p className="text-danger cursor-pointer" onClick={handleLogout}><i className="fa fa-sign-out-alt pt-2 text-danger"></i> Logout</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                            <h6>Notification</h6>
                            {notification?.messages.map((message)=><div className="card">
                                <div className="card-body">
                                    <h5 className="card-title">{message.title}</h5>
                                    <div className="d-flex justify-content-between">
                                        <p className="card-text">{message.message}</p>
                                    </div>
                                </div>
                            </div>)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <NewEventModal newEvent={newEvent} setNewEvent={setNewEvent} />
        </div>
    )
}

export default Index;
