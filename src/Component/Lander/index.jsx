import React, { useState } from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import Question from '../../Assets/Document/Event_Registration_Application.pdf';
import 'react-pdf/dist/Page/AnnotationLayer.css';
import styles from './style.module.scss';

const Index =  ({history}) => {
    const [numPages, setNumPages] = useState(null);
    const [page, setPage] = useState(1);

    const options = {
        cMapUrl: 'cmaps/',
        cMapPacked: true,
    };

    return (
        <div className={styles.container}>
            <nav className={`d-flex justify-content-end ${styles.navigation}`}>
            <button className="btn mr-4" onClick={() => history.push('./auth/login')}>Login</button>
            <button className="btn mr-2" onClick={() => history.push('./auth/signup')}>Signup</button>
            </nav>
            <Document
                file={Question}
                loading={'Loading...'}
                onLoadSuccess={({ numPages }) => setNumPages(numPages)}
                options={options}
                className={styles.document}
            >
                <Page pageNumber={page} width={1440} />
            </Document>
            {numPages && (
                <p>
                    Page {page} of {numPages}
                </p>
            )}
            {numPages && numPages > 1 && (
                <div className={styles.navigation}>
                    <button className="btn mb-5" onClick={() => setPage(page !== 1 ? page - 1 : 1)}>Back</button>
                    <button className="btn ml-2 mb-5" onClick={() => setPage(page !== numPages ? page + 1 : numPages)}>Next</button>
                </div>
            )}
        </div>
    );
};

export default Index;
