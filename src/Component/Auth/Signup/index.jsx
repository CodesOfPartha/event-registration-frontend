import React, { useState } from 'react';
import { postData } from '../../../Services/http_services';

const Index = ({history}) => {
    const [state, setState] = useState('');
    const [error, setError] = useState(false);
    const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const handleChange = (event) => {
        setState( event.target.value );
        if(event.target.value === ''){
            setError(false);
        }
    };
    
    const SignUp = () => {
        if (emailReg.test(state.trim())) {
            setError(false);
            postData('auth/signup', { email: state})
            .then((res)=>{
                alert('After verifying your account please login!');
                history.push('/login'); 
            })
            .catch((err)=>{console.log(err.response);history.push('/auth/login');})
        } else {
            setError(true);
        }
    }

    return (
        <div class="signin">
            <h2 className="mb-4 text-center mb-3">Signup</h2>
            <label for="username">Email</label>
            <input type="text" name="email" className="mb-1" value={state} onChange={handleChange}  placeholder=""/>
            {
                error?<p className="font-size-12 text-danger text-center mb-1"><i className="fa fa-info-circle mr-1"></i>Enter a valid email id!</p>:null
            }
            <div class="d-flex justify-content-end mb-3">
                <button class="btn" onClick={SignUp}>Free Sign up -></button>
            </div>
            <div className="d-flex justify-content-center">
                <button class="btn" onClick={()=>{history.push('/auth/login')}}>Sign in</button>
            </div>
        </div>
    )
}

export default Index;
