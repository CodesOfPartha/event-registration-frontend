import React from 'react';
import Login from './Login';
import Signup from './Signup';
import ForgotPassword from './ForgotPassword';
import './style.scss';

const Index = ({history, match}) => {

    const handleComponent = () => {
        switch(match.params.value){
            case 'login': return <Login history={history} />
            case 'signup': return <Signup history={history} />
            case 'forgot-password':return <ForgotPassword history={history} />
            default: history.push('/');
        }
    }

    return (
        <div>
            <div className="auth-container align-items-center">
                    <div class="content">
                        {handleComponent()}
                    </div>
                </div>
        </div>
    )
}

export default Index;
