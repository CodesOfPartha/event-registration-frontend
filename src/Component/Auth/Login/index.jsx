import React, {useState} from 'react';
import {postData} from '../../../Services/http_services';
import Cookies from "js-cookie";

const Index = ({history}) => {
    const [state, setState] = useState({email: "", password: ""});
    const [error, setError] = useState({email: false, password: false, api:false});
    const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const handleChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value
        });
        setError({email: false, password: false, api:false});
    };
    const SignIn = () => {
        if ( emailReg.test(state.email.trim()) && state.password !== '' ){
            setError({email: false, password: false, api:false});
            postData('user/login', state).then((res) => {
                localStorage.setItem("RefreshToken", JSON.stringify(res.data.refreshToken));
                postData(`auth-token`, {refreshToken: res.data.refreshToken})
              .then((res)=>{Cookies.set('AuthToken', res.data.authToken, {
                expires: 6,
                path: "/"
              });
              history.push('/dashboard/home');})
              .catch((err)=>console.log(err))
            }).catch((err) => setError({ ...error, api:true }))
        }else{
            if(emailReg.test(state.email.trim())){
                setError({
                    ...error, password:true
                })
            } else {
                setError({
                    ...error, email:true
                })
            }
        }
        
    }
    return (
        <div class="signin">
            <h2 className="mb-4 text-center">Continue the game!</h2>
            <label for="username">Email</label>
            <input type="text" name="email" className="mb-1" value={state.email} onChange={handleChange}  placeholder=""/>
            {
                error.email?<p className="font-size-12 text-danger text-center mb-1"><i className="fa fa-info-circle mr-1"></i>Enter a valid email id!</p>:null
            }
            <label for="password">Password</label>
            <input type="password" name="password" className="mb-1" value={state.password} onChange={handleChange}  placeholder=""/>
            {
                error.password?<p className="font-size-12 text-danger text-center mb-1"><i className="fa fa-info-circle mr-1"></i>Enter a valid password!</p>:
                error.api?<p className="font-size-12 text-danger text-center mb-1"><i className="fa fa-info-circle mr-1"></i>Enter a valid email & password!</p>:null
            }
            <div className="d-flex justify-content-end">
                <button class="btn" onClick={SignIn}>Sign in -></button>
            </div>
            <div class="d-flex justify-content-center mt-3">
                <button class="btn" onClick={()=>{history.push('/auth/signup')}}>Free Sign up -></button>
            </div>
        </div>
        )
    }
    
    export default Index;
