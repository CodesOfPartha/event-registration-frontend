import React, { useState, useEffect } from 'react';
import {getData, postData} from '../../../Services/http_services';
import Cookies from "js-cookie";

const Index = ({match, history}) => {
    const [state, setstate] = useState({ name:"", password:""})
    const verifyUser = () => {
        getData('auth/verify/'+match.params.token)
        .then((res)=>{
            localStorage.setItem('RefreshToken', JSON.stringify(res.data.refreshToken));
              postData(`auth-token`, {refreshToken: res.data.refreshToken})
              .then((res)=>Cookies.set('AuthToken', res.data.authToken, {
                expires: 6,
                path: "/"
              }))
              .catch((err)=>console.log(err))
        })
        .catch((err)=>console.log(err))
    }

    const updateUserInfo = () => {
        postData(`user/update`,state)
        .then((res)=>history.push('/dashboard'))
        .catch((err)=>console.log(err))
    }

    useEffect(() => {
        verifyUser();
    }, [])
    return (
        <div>
            <div className="auth-container align-items-center">
                    <div class="content">
                    <div class="signin">
                        <h2 className="mb-4 text-center mb-3">Welcome to event <br /> registration app!</h2>
                        <label for="username">Name</label>
                        <input type="text" value={state.name} name="name" className="mb-1" onChange={(event)=>setstate({name:event.target.value, password:state.password})}  placeholder=""/>
                        <label for="username">Password</label>
                        <input type="password" value={state.password} name="password" onChange={(event)=>setstate({name:state.name, password:event.target.value})} className="mb-1"  placeholder=""/>
                        <div class="d-flex justify-content-center mt-3 mb-3">
                            <button class="btn" onClick={updateUserInfo} disabled={ state.name ==='' || state.password ===''} >Update user info</button>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
    )
}

export default Index;
