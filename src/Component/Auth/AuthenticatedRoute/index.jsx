import React from 'react';
import { Route } from "react-router-dom";

const Index = ({ component, history, ...rest }) => {
    const RefreshToken = JSON.parse(localStorage.getItem("RefreshToken"));
    const loginStatus = RefreshToken? true:false;
    if (!loginStatus){
        return (
            window.open('/','_self')
        );
    }

  return <Route component={component} {...rest} />;

}

export default Index;
