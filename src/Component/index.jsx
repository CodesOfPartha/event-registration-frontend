export { default as Auth } from './Auth';
export { default as Dashboard } from './Dashboard';
export { default as Lander } from './Lander';
export { default as VerifyUser } from './Auth/VerifyUser';