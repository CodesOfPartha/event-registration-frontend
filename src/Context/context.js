import React, { createContext, useState, useContext } from "react";

export const CommonContext = createContext();

export const CommonProvider = ({ children }) => {
    const [admin, setAdmin] = useState(false);
    const [appData ,setAppData] = useState({
        isLoggedIn : false,
        role : ""
    });
    const [authToken, setAuthToken] = useState(undefined)


    return (
        <CommonContext.Provider value={{ admin, setAdmin, appData, setAppData, authToken, setAuthToken }}>
            {children}
        </CommonContext.Provider>
    );
};

export const useCommonContext = () => {
    return useContext(CommonContext);
};
