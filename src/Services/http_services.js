import axios from "axios";
import Cookies from "js-cookie";

const apiPath = "http://localhost:8080/api/";

function handleHeaders() {
  let headers = {
    "app-version": "1",
    "content-type": "application/json",
    "device-type": "3"
  }
  const Authtoken = Cookies.get('AuthToken');
  if(Authtoken){
    headers['Authorization'] = Authtoken;
  }
  return headers;
}

const getAuthToken = () => {
  return new Promise(function (resolve, reject) {
    const RefreshToken = JSON.parse(localStorage.getItem('RefreshToken'));
    postData(`auth-token`, {refreshToken: RefreshToken})
      .then(res => {
        Cookies.set('AuthToken', res.data.authToken, {
          expires: 6,
          path: "/"
        });
        resolve(true);
      })
      .catch(err => {
        reject(false)
      });
  });
}


export const postData = (path, data, auth = null) => {
    const headers = handleHeaders();
    return new Promise(function (resolve, reject) {
      axios({
        method: "post",
        responseType: "json",
        url: apiPath + path,
        data: data,
        auth: auth,
        headers: headers
      })
        .then(res => {
          return resolve(res);
        })
        .catch(error => {
          if (error.response.status === 401) {
            getAuthToken().then((res) => {
              resolve(postData(path, data, auth));
            })
          } else {
            return reject(error);
          }
        });
    });
  };
  
  export const getData = (path, auth = null) => {
    const headers = handleHeaders();
    return new Promise(function (resolve, reject) {
      axios({
        method: "get",
        responseType: "json",
        url: apiPath + path,
        auth: auth,
        headers: headers
      })
        .then(res => {
          return resolve(res);
        })
        .catch(error => {
          if (error.response.status === 401) {
            getAuthToken().then((res) => {
              resolve(getData(path, auth));
            })
          } else {
            return reject(error);
          }
        });
    });
  };
  
  export const deleteData = (path, data = null) => {
    const headers = handleHeaders();
    return new Promise(function (resolve, reject) {
      axios({
        method: "delete",
        responseType: "json",
        url: apiPath + path,
        data: data,
        headers: headers
      })
        .then(res => {
          return resolve(res);
        })
        .catch(error => {
            return reject(error);
        });
    });
  };
  
  
  export const putData = (path, data, auth = null) => {
    const headers = handleHeaders();
    return new Promise(function (resolve, reject) {
      axios({
        method: "put",
        responseType: "json",
        url: apiPath + path,
        data: data,
        auth: auth,
        headers: headers
      })
        .then(res => {
          return resolve(res);
        })
        .catch(error => {
            return reject(error);
        });
    });
  };// patch
  
  export const patchData = (path, data, auth = null) => {
    const headers = handleHeaders();
    return new Promise(function (resolve, reject) {
      axios({
        method: "patch",
        responseType: "json",
        url: apiPath + path,
        data: data,
        auth: auth,
        headers: headers
      })
        .then(res => {
          return resolve(res);
        })
        .catch(error => {
            return reject(error);
        });
    });
  };
