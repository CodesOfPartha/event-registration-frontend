import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Auth, Dashboard, Lander, VerifyUser } from './Component';
import { CommonProvider } from './Context/context';
import AuthenticatedRoute from './Component/Auth/AuthenticatedRoute';

function App() {
  return (
    <div>
      <Router>
        <div className="App">
          <Switch>
          <Route path='/' exact component={Lander} />
            <Route path='/auth/:value' component={Auth} />
            <Route path='/verify/:token' component={VerifyUser} />
            <AuthenticatedRoute path='/dashboard'  >
              <CommonProvider>
                <Dashboard />
              </CommonProvider>
            </AuthenticatedRoute>
            <Redirect to="/" />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
